silent! call pathogen#infect()

try
  colorscheme solarized
  let g:solarized_termcolors=256
  set t_Co=256
catch /^Vim\%((\a\+)\)\=:E185/
  set t_Co=16
endtry

set background=dark

" enable syntax highlighting and indenting
syntax on
filetype plugin indent on

" custom colours
highlight Normal cterm=NONE ctermbg=0
highlight LineNr cterm=NONE ctermfg=10 ctermbg=0
highlight CursorLineNr cterm=NONE ctermfg=14 ctermbg=0
highlight SignColumn term=NONE cterm=NONE ctermfg=10 ctermbg=0
highlight Visual term=NONE cterm=NONE ctermfg=0 ctermbg=6
highlight GitGutterAddInvisible ctermfg=7 ctermbg=0
highlight GitGutterChangeInvisible ctermfg=7 ctermbg=0
highlight GitGutterDeleteInvisible ctermfg=7 ctermbg=0
highlight GitGutterAdd ctermfg=2 ctermbg=0
highlight GitGutterChange ctermfg=3 ctermbg=0
highlight StatusLine cterm=NONE ctermfg=10 ctermbg=8
highlight StatusLineNC cterm=NONE ctermfg=0 ctermbg=8
highlight VertSplit cterm=NONE ctermfg=0 ctermbg=8
highlight StatusLineTerm cterm=NONE ctermfg=10 ctermbg=8
highlight StatusLineTermNC cterm=NONE ctermfg=0 ctermbg=8
highlight CtrlPMode1 cterm=NONE ctermfg=14 ctermbg=8
highlight CtrlPMode2 cterm=NONE ctermfg=14 ctermbg=8

" autoformat comments by default
set textwidth=80
set formatoptions=croqj
set nojoinspaces

" autoformat text for various text files
autocmd BufRead,BufNewFile *.md setlocal formatoptions+=t

" toggle paste mode with \z to avoid autoformatting
inoremap <silent> <Leader>z <esc>:set paste!<cr>i
nnoremap <silent> <Leader>z :set paste!<cr>

" enable line numbers
set number
set cursorline
set cursorlineopt=number

" disable backups
set nobackup
set noundofile

" disable bell
set visualbell
set t_vb=

" set tabs
set softtabstop=2
set shiftwidth=2
set smarttab
set expandtab

" set tab completion menu
set wildmenu
set wildignorecase
set wildignore+=*.so,*.swp,*.zip
set wildmode=longest:full,full
set wildcharm=<Tab>
set path=.,**

" partially enable mouse
set mouse=nv

" navigation for splits
nnoremap <C-H> <C-W><C-H>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-Left> <C-W><C-H>
nnoremap <C-Down> <C-W><C-J>
nnoremap <C-Up>   <C-W><C-K>
nnoremap <C-Right> <C-W><C-L>

" go to previous window and make it the only window, closing all others - useful
" for closing help and git status windows
nmap <leader>o <C-W>p<C-W>o

" get the current git branch
function! GitBranch()
  let s:branch=trim(system("git -C " . expand("%:h") . " branch --show-current 2>/dev/null"))
  if s:branch != ""
    return "(" . s:branch . ")"
  else
    return ""
  end
endfunction

augroup GitBranch
  autocmd!
  autocmd BufWinEnter * let b:git_branch = GitBranch()
augroup END

" get the current git root
function! GitRoot()
  let s:root=trim(system("git -C " . expand("%:h") . " rev-parse --show-toplevel 2>/dev/null"))
  if s:root != ""
    return s:root
  else
    return expand("%:p:h")
  end
endfunction

augroup GitRoot
  autocmd!
  autocmd BufWinEnter * let b:git_root = GitRoot()
augroup END

" set the status line
set laststatus=2
set statusline=
set statusline+=%{b:git_branch}
set statusline+=\ %t
set statusline+=\ [%{&ff}]
set statusline+=\ [%{&fileencoding?&fileencoding:&encoding}]
set statusline+=%{&paste?'\ [paste]':''}
set statusline+=\ %m%r%h
set statusline+=%=
set statusline+=%{b:git_root}
set statusline+=\ %l,%c\ of\ %L\ 

" prettify netrw
let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 0
let g:netrw_winsize = 25
let g:netrw_list_hide= netrw_gitignore#Hide().',.*\.swp$,.*\.git$,^\.git/$,.*\.gitmodules,.*\.netrwhist'
let g:netrw_keepdir = 0

" toggle netrw
nnoremap <leader>e :Explore<CR>
nnoremap <leader>l :Lexplore<CR>

""" FIXES

" stop vi starting in replace mode in WSL
nnoremap <esc>^[ <esc>^[

" show netrw when vim starts with no argument
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') && v:this_session == '' |
      \ Explore |
      \ endif

" exit vim if netrw is the only window or tab remaining
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw" |
      \ quit |
      \ endif
autocmd BufEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&filetype") == "netrw" |
      \ quit |
      \ endif

" stop netrw from leaving [No Name] buffers and make sure other buffers continue
" to be hidden to avoid save warnings
set nohidden
autocmd FileType netrw setl bufhidden=wipe
augroup netrw_buf_hidden_fix
    autocmd!
    autocmd BufWinEnter *
                \  if &ft != 'netrw'
                \|     set bufhidden=hide
                \| endif
augroup END

" fix git commit buffers not working with paragraph formatting
augroup netrw_gitcommit_fo_fix
    autocmd!
    autocmd BufWinEnter *
                \  if &ft != 'gitcommit'
                \|     setlocal formatoptions+=a
                \| endif
augroup END

""" PLUGINS

""" vim rooter

let g:rooter_cd_cmd = 'lcd'
let g:rooter_silent_chdir = 1

""" git gutter

set signcolumn=yes
set updatetime=100
nmap <leader>n <Plug>(GitGutterNextHunk)
nmap <leader>a <Plug>(GitGutterStageHunk)
nmap <leader>hs <Nop>
nmap <leader>hu <Nop>

""" vim fugitive

nmap <leader>s :Git -p status<RETURN>
nmap <leader>d :Git -p diff<RETURN>
nmap <leader>D :Git -p diff --staged<RETURN>
nmap <leader>c :Git commit<RETURN>
nmap <leader>C :Git commit -a<RETURN>
nmap <leader>p :Git -p push<RETURN>

""" tabular

nmap <leader>t :Tabularize /=><Return>

""" ctrlp

let g:ctrlp_working_path_mode = 'rwa'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
nnoremap f :CtrlP<CR>
nnoremap F :CtrlPMRU<CR>
nnoremap <Tab> :CtrlPBuffer<CR>

" turn on the cursorline when in ctrlp
function! CtrlPSetCursorLine()
  set cursorlineopt=line
endfunction
function! CtrlPUnsetCursorLine()
  set cursorlineopt=number
endfunction
let g:ctrlp_buffer_func = { 'enter': 'CtrlPSetCursorLine', 'exit':  'CtrlPUnsetCursorLine', }
