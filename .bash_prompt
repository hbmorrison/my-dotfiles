# Set shell prompt colours.

export PROMPT_COLOUR_RED='\[\033[00;31m\]'
export PROMPT_COLOUR_GREEN='\[\033[00;32m\]'
export PROMPT_COLOUR_YELLOW='\[\033[00;33m\]'
export PROMPT_COLOUR_PURPLE='\[\033[00;35m\]'
export PROMPT_COLOUR_CYAN='\[\033[00;36m\]'
export PROMPT_COLOUR_CLEAR='\[\033[00m\]'

# Set up git prompt.

case $SHELL_ENVIRONMENT in
  chromeos|wsl)
    source /usr/lib/git-core/git-sh-prompt
    ;;
esac

GIT_PROMPT="$PROMPT_COLOUR_CYAN\$(__git_ps1)$PROMPT_COLOUR_CLEAR"

# Set up directory prompt.

DIR_PROMPT="$PROMPT_COLOUR_YELLOW\W$PROMPT_COLOUR_CLEAR"

# Make sure the hostname is lowercase.

HOSTNAME=`echo $HOSTNAME | tr '[:upper:]' '[:lower:]'`
if [ $SHELL_ENVIRONMENT = "wsl" ]
then
  PROMPT_HOSTNAME="${HOSTNAME}|wsl"
else
  PROMPT_HOSTNAME=$HOSTNAME
fi

# Set up a window title prompt.

TITLE_PROMPT="\[\e]0;\u@${HOSTNAME}\$(__git_ps1) \W\a\]"

# Set the entire prompt.

PS1="${TITLE_PROMPT}${debian_chroot:+($debian_chroot)}${PROMPT_COLOUR_GREEN}\u@${PROMPT_HOSTNAME}${PROMPT_COLOUR_CLEAR}${GIT_PROMPT} ${DIR_PROMPT} \\$ "
